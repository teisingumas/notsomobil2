class Rod:
    def __init__(self, ll, rl):
        self.lengthL = ll
        self.lengthR = rl
        self.leftEntity = None
        self.rightEntity = None
        self.conAmount = 0  # connected entities

    def SetLeftEntity(self, ntity):
        self.leftEntity = ntity
        self.conAmount += 1

    def SetRightEntity(self, ntity):
        self.rightEntity = ntity
        self.conAmount += 1

    def Weight(self):
        return self.leftEntity.Weight() + self.rightEntity.Weight()

    def IsBalanced(self):
        lw = self.lengthL * self.leftEntity.Weight()
        rw = self.lengthR * self.rightEntity.Weight()
        bal = True
        if (type(self.leftEntity) is Rod):
            bal = self.leftEntity.IsBalanced()
            if not bal:
                return bal
        if (type(self.rightEntity) is Rod):
            bal = self.rightEntity.IsBalanced()
        return lw == rw and bal