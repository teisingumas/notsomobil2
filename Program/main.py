# 839
from Rod import Rod
from Ball import Ball

def GetBalancedText(rod):
    if rod != None: # and there was a top rod
        if rod.IsBalanced(): # then we can check if it was balanced
            return "YES"
    return "NO"

def CheckFileForBalance(fname):
    topRod = None
    stack = []
    f = open(fname,"r")
    d = f.readline()
    while d:
        if d == '\n': # if empty line
            if topRod != None:
                print(GetBalancedText(topRod))
            topRod = None
            stack = []
        if len(d) > 2:
            d = d.replace('\n', '')
            splt = d.split(' ')
            r = Rod(int(splt[1]), int(splt[3])) # creating a rod with left length and right length
            lbw = int(splt[0]) # left ball weight
            if lbw > 0:        # if weight is bigger than zero then its a ball
                b = Ball(lbw)
                r.SetLeftEntity(b)
            rbw = int(splt[2]) # right ball weight
            if rbw > 0:        # if weight is bigger than zero then its a ball
                b = Ball(rbw)
                r.SetRightEntity(b)
            if topRod == None:
                topRod = r
            if len(stack) > 0:
                tr = stack[-1] # getting rod on top of stack
                if tr.leftEntity == None: # setting left entity if needed
                    tr.SetLeftEntity(r)
                elif tr.rightEntity == None: # setting right entity if needed and then removing from stack
                    tr.SetRightEntity(r)     # because both sides are occupied
                if tr.conAmount == 2:
                    stack.pop()
            if r.conAmount != 2:  # if current rod is not full then it is added to stack
                stack.append(r)
        d = f.readline()
    print(GetBalancedText(topRod))

CheckFileForBalance("data2.txt")